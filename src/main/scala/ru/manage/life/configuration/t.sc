import scala.annotation.tailrec
//def maxScore(cardPoints: Array[Int], k: Int): Int = {
//  val length = cardPoints.length
//  if(length == k){
//    cardPoints.sum
//  } else {
//    def go(array: Array[Int], k: Int, acc: Int): Int = {
//      if(k == 0) acc
//      else {
//        val left = array.head
//        val right = array.last
//        math.max(go(array.drop(1), k - 1, acc + left), go(array.dropRight(1), k - 1, acc + right))
//      }
//    }
//    go(cardPoints, k, 0)
//  }
//}
//maxScore(Array(1,2,3,4,5,6,1), 3)
//maxScore(Array(2,2,2), 2)
//maxScore(Array(9,7,7,9,7,7,9), 7)
//maxScore(Array(1,1000,1), 1)
//maxScore(Array(1,79,80,1,1,1,200,1), 3)
//maxScore(Array(9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1), 10)
//maxScore(Array(11,49,100,20,86,29,72), 4)
//
////12
////4
////55
////1
////202
////1820
//
//////0 1 2
//////5 4 3
//////
//////0 5 - 1
//////0 4 - 2
//////0 3 - 3
//////1 5 - 4
//////1 4 - 5
//////1 3 - 6
//////2 5 - 7
//////2 4 - 8
//////2 3 - 9
//////0 5 1 4
//////0 5 1 3
//////0 5 2 4
//////0 5 2 3
//////1 5 2 4
//////1 5 2 3
////val a = BigDecimal(38,2)
////val first = Array(0,1,2)
////val second = Array(5,4,3)
////val res = for {
////  i <- first.indices
////  j <- second.indices
////} yield Seq((i, j))
////
////res
////
////val res2 = for {
////  i <- res.indices
////  j <- res.indices
////} yield (res(i), res(j))
////
//////res2.


case class MyList[A](head: A, tail: Option[MyList[A]]) {
  @tailrec
  final def doSwap(stack: MyList[A], result: MyList[A]): MyList[A] = {
    stack.tail match {
      case None => MyList(stack.head, Some(result))
      case Some(el) => doSwap(el, MyList(stack.head, Some(result)))
    }
  }

  def reverse: MyList[A] = {
    tail match {
      case None => MyList(head, None)
      case Some(tail) => doSwap(tail, MyList(head, None))
    }
  }
}

MyList[Int](1, Some(MyList[Int](2, None))).reverse