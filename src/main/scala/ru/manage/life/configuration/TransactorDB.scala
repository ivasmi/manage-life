package ru.manage.life.configuration
//import doobie.util.ExecutionContexts
//import doobie.util.transactor.Transactor
import cats.effect.{Blocker, IO, Resource}
import doobie.h2._
import doobie.util.ExecutionContexts
import io.getquill.Literal
//import doobie.util.ExecutionContexts
import io.getquill.{idiom => _}
import doobie.quill.DoobieContext

object TransactorDB {

//  implicit val cs = IO.contextShift(ExecutionContexts.synchronous)
  implicit val cs: DoobieContext.H2[Literal.type] = new doobie.quill.DoobieContext.H2(Literal)
//  lazy val ctx1: H2JdbcContext[CamelCase.type] = new H2JdbcContext(CamelCase, "ctx")

//  lazy val ctx: Aux[IO, Unit] = Transactor.fromDriverManager[IO](
//    driver = "org.h2.jdbcx.JdbcDataSource",
//    url = "jdbc:h2:~/User/ivansmirnov/database/myproject",
//    user = "root",
//    pass = "root",
//    blocker = Blocker.liftExecutionContext(ExecutionContexts.synchronous)
//  )



//  val xa = Transactor.fromDriverManager[IO](
//    driver = "org.h2.jdbcx.JdbcDataSource",
//    url = "jdbc:h2:~/User/ivansmirnov/database/myproject",
//    user = "root",
//    pass = "root",
//    blocker = Blocker.liftExecutionContext(ExecutionContexts.synchronous)
//  )


  val transactor: Resource[IO, H2Transactor[IO]] =
    for {
      ce <- ExecutionContexts.fixedThreadPool[IO](32) // our connect EC
      be <- Blocker[IO]    // our blocking EC
      xa <- H2Transactor.newH2Transactor[IO](
        url = "jdbc:h2:~/User/ivansmirnov/database/myproject;DB_CLOSE_DELAY=-1",   // connect URL
        user = "root",                                   // username
        pass = "root",                                     // password
        connectEC = ce,                                     // await connection here
        blocker = be                                      // execute JDBC operations here
      )
    } yield xa

}
