//import scala.collection.immutable
////def canBeEqual(target: Array[Int], arr: Array[Int]): Boolean = {
////  val inputArr = arr.sorted
////  target.sorted.zipWithIndex.map{ case (el, index) =>
////    inputArr(index) == el
////  }.reduce(_ && _)
////}
//
//def hasAllCodes(s: String, k: Int): Boolean = {
//  if(s.length < k) false else {
//    def generateBinaryCode: Boolean = {
//      val maxValue = (for{
//        i <- 0 until  k
//      } yield math.pow(2,i)).sum.toInt
//      Stream.range(0, maxValue+1).find{ el =>
//        val elBin = el.toBinaryString
//        val retStr = Array.fill(k - elBin.length)(0).mkString("").concat(elBin)
//        !(s.contains(retStr))
//      } match {
//        case None => true
//        case _ => false
//      }
//    }
//    generateBinaryCode
//  }
//}
//
//hasAllCodes("00110110", 2)
//
//
//hasAllCodes("01001000100111101001010101110100010001011100011100100101010000001101010101110100100011010110101000011111111111101000010010000001000111111001110010000001011010001110100010001010001110010111011010110101110110110011010001001000110001101010101010111011111000010110101101100010000001001110000000000001100110111001011010100101001011111110010010001100011100101110111001100101001011100001110",7)
//
//hasAllCodes("011101100101110101101000011111101011111101110100111100010000010110010010011100110001110010101101011010010001101111000111110000001010100101111001111010110001111011001110100010001111000111010001111100101011100001001011101100010101010110001011110101001101001001111101000100011101110100100100101101110000000110001011100100111111001000100100010011001000101101100010010010001111010111010011110111110001010100000110000111010110001100100110111000111010111000010100100100101011001111010110010101110101000011011101000110001001100111100011000100110010101100001111000100101001111001100001010100100100110100101100111000110010110101010110010110001111010110101111011011100111001010101001011000101101110100001110011110001011000011100011111001110011111101110001110010000111010011110001011010100101110010110110100011111011110010100011111000000001011100110000010101110110111",7)


def checkIfPrerequisite(n: Int, prerequisites: Array[Array[Int]], queries: Array[Array[Int]]): Array[Boolean] = {
  if(prerequisites.isEmpty) Array.fill(queries.length)(false) else {
    queries.map { query =>
      var cach = 0
      prerequisites.zipWithIndex.map{ case (preq, ind) =>
        if(ind == 0){
          cach = preq(0)
          println(preq(0) == query(0), n - ind - 1 )
          (preq(0) == query(0), n - ind - 1 )
        } else {
          if(preq(0) != cach) (false, n-ind-1) else {
            println(query(1) == preq(1), n-ind-1)
            (query(1) == preq(1), n-ind-1)
          }
        }
      }.foldLeft((false, true)){ case (acc, bin) =>
        println(acc._1 || bin._1, (bin._2 >= 0) && acc._2)
        (acc._1 || bin._1, (bin._2 >= 0) && acc._2)
      }
    }.map(el => el._1 && el._2)
  }
}

checkIfPrerequisite(2, Array(Array(1,0)), Array(Array(0,1),Array(1,0)))
checkIfPrerequisite(2, Array.empty, Array(Array(0,1),Array(1,0)))

checkIfPrerequisite(3, Array(Array(1,2),Array(1,0),Array(2,0)), Array(Array(1,0),Array(1,2)))