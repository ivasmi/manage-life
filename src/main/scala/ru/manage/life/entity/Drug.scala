package ru.manage.life.entity

import java.sql.Timestamp

case class Drug(id: String,
               barCode: String,
               name: String,
               systemEntryDate: Timestamp,
               expiryDate: Timestamp,
               dosage: Int,
               number: Int)
