package ru.manage.life.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ru.manage.life.dao.DrugDAO

import scala.concurrent.Future
import scala.util.{Failure, Success}

class DrugRoutes(drugService: DrugDAO) {

  val routes: Route = {
    pathPrefix("drug"){
      path("list") {
        get {
          onSuccess(Future.successful(Seq(1, 2, 3, 4, 5, 6))) { drugs =>
            complete(drugs.mkString("drugs: ",", ", ""))
          }
        }
      } ~
      post { //insert
        entity(as[String]){ newDrug =>
          onSuccess(Future.successful (newDrug)) { savedDrug =>
            complete(StatusCodes.Created, savedDrug)
          }
        }
      } ~
      put { //update
        entity(as[String]){ updateDrug =>
          onSuccess(Future.successful(updateDrug)) { updatedDrug =>
            complete(StatusCodes.Created, updatedDrug)
          }
        }
      } ~
      get { //get by id
        path(Segment) { id =>
          rejectEmptyResponse {
            drugService.select(id) match {
              case None => complete(StatusCodes.NotFound, "fuck")
              case Some(drug) => complete(StatusCodes.OK, drug.toString)
            }
          }
        }
      } ~
      delete {
        path(Segment) { id =>
          rejectEmptyResponse {
            onComplete(Future.successful(drugService.delete(id))) {
              case Success(_) => complete(StatusCodes.OK, "Complete Delete")
              case Failure(ex) => complete(StatusCodes.NotFound, ex)
            }
          }
        }
      }
    }
  }
}
