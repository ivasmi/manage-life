package ru.manage.life

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.{Route, RouteResult}
import akka.http.scaladsl.server.Directives._
import org.flywaydb.core.Flyway

import scala.util.Failure
import scala.util.Success
import configuration.Config
import ru.manage.life.dao.{DrugDAOImpl, InjectionDAOImpl, ReasonDAOImpl}
import ru.manage.life.routes.{DrugRoutes, InjectionRoutes, ReasonRoutes}

//#main-class
object QuickstartApp  {
  //#start-http-server
  private def startHttpServer(routes: Route, system: ActorSystem[_]): Unit = {
    // Akka HTTP still needs a classic ActorSystem to start
    implicit val classicSystem: akka.actor.ActorSystem = system.toClassic
    import system.executionContext

    val futureBinding = Http().bindAndHandle(RouteResult.route2HandlerFlow(routes), Config.host, Config.port)
    futureBinding.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
        system.log.info("Server online at http://{}:{}/", address.getHostString, address.getPort)
      case Failure(ex) =>
        system.log.error("Failed to bind HTTP endpoint, terminating system", ex)
        system.terminate()
    }
  }
  //#start-http-server
  def main(args: Array[String]): Unit = {
    //migration database

    println(Config.port, Config.host, Config.dbPassword, Config.dbUser, Config.dbUrl)
    val flyway = Flyway.configure().dataSource(Config.dbUrl, Config.dbUser, Config.dbPassword).load()
    flyway.migrate()

    //#server-bootstrapping
    val rootBehavior = Behaviors.setup[Nothing] { context =>
      val routes =
        new DrugRoutes(new DrugDAOImpl()).routes ~
        new ReasonRoutes(new ReasonDAOImpl()).routes ~
        new InjectionRoutes(new InjectionDAOImpl()).routes
      startHttpServer(routes, context.system)

      Behaviors.empty
    }
    val system = ActorSystem[Nothing](rootBehavior, "AkkaHttpServer")
    //#server-bootstrapping
  }
}
//#main-class
