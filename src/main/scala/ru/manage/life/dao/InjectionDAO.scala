package ru.manage.life.dao

import io.getquill.{idiom => _}
import ru.manage.life.entity.Injection

import ru.manage.life.configuration.TransactorDB.transactor

trait InjectionDAO {
  def insert(injection: Injection): Injection
  def delete(id: String): Unit
  def update(injection: Injection): Injection
  def select(id: String): Option[Injection]
  def list(): Seq[Injection]
}

class InjectionDAOImpl extends InjectionDAO {


  private val injection = quote(querySchema[Injection]("Injection"))

  override def insert(entity: Injection): Injection = {
    transactor {
      run(injection.insert(lift(entity))).flatMap { _ =>
        run(injection.filter(_.id == lift(entity.id)))
      }
    }
  }

  override def delete(id: String): Unit = {
    run(injection.filter(_.id == lift(id)).delete)
  }

  override def update(entity: Injection): Injection = {
    val result = runIO(injection.update(lift(entity))).flatMap{ _ =>
      runIO(injection.filter(_.id == lift(entity.id)))
    }
    performIO(result).head
  }

  override def select(id: String): Option[Injection] = {
    run(injection.filter(c => c.id == lift(id))).headOption
  }

  def list(): Seq[Injection] = {
    run(injection)
  }

}